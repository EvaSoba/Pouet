package fr.scholanova.eial.archidist.PresentationEjb;

import javax.ejb.Remote;
import javax.ejb.Singleton;

import fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface;
import fr.scholanova.eial.archidist.PresentationEjbInterface.Repas;

@Remote(MangerInterface.class)
@Singleton
public class Manger implements MangerInterface {

	@Override
	public Repas mangerDehors() {
		Repas repasDehors = new Repas("salade", 100, true);
		return repasDehors;
	}
	


}
