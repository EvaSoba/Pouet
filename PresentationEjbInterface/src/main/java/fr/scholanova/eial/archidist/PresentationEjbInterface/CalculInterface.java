package fr.scholanova.eial.archidist.PresentationEjbInterface;

public interface CalculInterface {
	
	public int ajouter(int i, int j);
	public double puissance(double nombre, double exposant);
}