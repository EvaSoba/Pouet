package fr.scholanova.eial.archidist.PresentationEjbInterface;

import java.io.Serializable;

public class Repas implements Serializable {
	private String nom;
	private double prix;
	private boolean dehors;
	
	public Repas(){
		super();
	}
	
	public Repas(String nom, double prix, boolean dehors) {
		super();
		this.setNom(nom);
		this.setPrix(prix);
		this.setDehors(dehors);
	}
	
	public String toString() {
		return "Repas : " + this.nom + "\n prix : " + this.prix + " Dehors : " + this.dehors;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public boolean isDehors() {
		return dehors;
	}

	public void setDehors(boolean dehors) {
		this.dehors = dehors;
	}
}
