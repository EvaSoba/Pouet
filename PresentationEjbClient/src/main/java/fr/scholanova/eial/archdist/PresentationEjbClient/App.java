package fr.scholanova.eial.archdist.PresentationEjbClient;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface;
import fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface;
/**
 * Hello world!
 *
 */
public class App 
{
	
	
    public static void main( String[] args )
    {
    	Properties ppt = new Properties();
        ppt.put(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8080");
        ppt.put(Context.SECURITY_PRINCIPAL, "admin");
        ppt.put(Context.SECURITY_CREDENTIALS, "password");
        ppt.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        ppt.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        ppt.put("jboss.naming.client.ejb.contex", true);
        
        Context ctx;
		try {
			ctx = new InitialContext(ppt);
			CalculInterface proxy = (CalculInterface) ctx.lookup(
        		"PresentationEjbEAR-1.0/PresentationEjb-1.0/Calcul!fr.scholanova.eial.archidist.PresentationEjbInterface.CalculInterface");
			System.out.println(proxy.ajouter(1, 2));
			System.out.println(proxy.puissance(5, 2));
			
			ctx = new InitialContext(ppt);
			MangerInterface proxy2 = (MangerInterface) ctx.lookup(
        		"PresentationEjbEAR-1.0/PresentationEjb-1.0/Manger!fr.scholanova.eial.archidist.PresentationEjbInterface.MangerInterface");
			
			System.out.println(proxy2.mangerDehors().toString());
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
